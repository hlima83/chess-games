package com.chess;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private Long gameId;
    private List<Player> players;

    public Game(Long gameId) {
        this.gameId = gameId;
        this.players = new ArrayList<>();
    }

    public Game(Long gameId, List<Player> players) {
        this.gameId = gameId;
        this.players = players;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
