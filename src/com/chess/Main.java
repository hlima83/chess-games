package com.chess;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        Map<Long, Game> games = new HashMap<>();

        //players
        Player player1 = new Player("player1");
        Player player2 = new Player("player2");
        Player player3 = new Player("player3");
        Player player4 = new Player("player4");
        Player player5 = new Player("player5");
        Player player6 = new Player("player6");
        Player player7 = new Player("player7");

        List<Player> playersList1 = Arrays.asList(player1, player2);
        List<Player> playersList2 = Arrays.asList(player2, player4);
        List<Player> playersList3 = Arrays.asList(player2, player5);
        List<Player> playersList4 = Arrays.asList(player6, player7);

        //games
        Game game1 = new Game(1l, playersList1);
        games.put(1l, game1);
        Game game2 = new Game(2l, playersList2);
        games.put(2l, game2);
        Game game3 = new Game(3l, playersList3);
        games.put(3l, game3);
        Game game4 = new Game(4l, playersList4);
        games.put(4l, game4);

        //players friends games
        Set<Long> player1FriendsGames = new HashSet<>();
        player1FriendsGames.add(2l);
        player1.setFriendGameIds(player1FriendsGames);
        Set<Long> player2FriendsGames = new HashSet<>();
        player2FriendsGames.add(1l);
        player2FriendsGames.add(3l);
        player2.setFriendGameIds(player2FriendsGames);

        //find players friends/opponents

        List<String> players = games.values().stream().map(g -> g.getPlayers()).flatMap(List::stream)
                .map(p -> p.getFriendGameIds()).flatMap(Set::stream).map(id -> games.get(id).getPlayers())
                .flatMap(List::stream).map(p -> p.getUsername()).collect(Collectors.toSet())
                .stream().sorted().collect(Collectors.toList());

        players.forEach(p -> System.out.println(p));
    }
}
