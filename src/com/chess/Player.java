package com.chess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Player {

    private String username;
    private Set<Long> friendGameIds;

    public Player(String username) {
        this.username = username;
        this.friendGameIds = new HashSet<>();
    }

    public Player(String username, Set<Long> friendGameIds) {
        this.username = username;
        this.friendGameIds = friendGameIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Long> getFriendGameIds() {
        return friendGameIds;
    }

    public void setFriendGameIds(Set<Long> friendGameIds) {
        this.friendGameIds = friendGameIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(username, player.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }


}
